function button(text) {
    this.text = text;
    var button = document.createElement('button');
    button.className = 'button';
    button.textContent = this.text;
    button.setAttribute('disable', true);
    document.body.appendChild(button);
}

button('button',true);