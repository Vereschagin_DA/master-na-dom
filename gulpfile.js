require('./gulp/tasks/pug.js');
require('./gulp/tasks/styles.js');
require('./gulp/tasks/watch.js');
require('./gulp/tasks/copy.js');
require('./gulp/tasks/clean.js');
require('./gulp/tasks/image-min.js');
require('./gulp/tasks/build.js');